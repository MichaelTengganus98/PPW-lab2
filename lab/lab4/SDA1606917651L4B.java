import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.ArrayList;

public class SDA1606917651L4B{
public static ArrayList<String> arr= new ArrayList<String>();

	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String str=in.readLine();
		while(str!=null){
			String[] strdex = str.split(" ");
			if(strdex[0].equals("PICK")){
				pick(strdex[1]+" "+strdex[2]);
			}
			else if(strdex[0].equals("ATTACK")){
				attack();
			}
			else if(strdex[0].equals("DEFENSE")){
				defense();
			}
			else if(strdex[0].equals("SEE")&&strdex[1].equals("CARD")){
				seeCard();
			}
			str=in.readLine();
			
		}
	}
	public static void attack(){
		if(arr.size()>0){
			System.out.println(arr.get(0)+" dikeluarkan");
			arr.remove(0);
		}
		else{
			System.out.println("Tidak bisa melakukan Attack");	
		}
	}
	
	public static void defense(){
		if(arr.size()>2){
			int i=0;
			while (i<3){
				System.out.println(arr.get(arr.size()-1)+" dikeluarkan");
				arr.remove(arr.size()-1);
				i++;
			}
		}
		else{
			System.out.println("Tidak bisa melakukan Defense");			
		}
	}
	
	public static void pick(String str){
		String[] print=str.split(" ");
		System.out.println(print[0]+" dengan power "+print[1]+" diambil");
		arr.add(str);
		quicksort(0,arr.size()-1);
	}
	
	public static void seeCard(){
		if(arr.size()!=0){
			for(int i=0;i<arr.size();i++){
				System.out.println(arr.get(i));
			}
		}
		else{
			System.out.println("Kartu kosong");
		}
	}
	
	public static void quicksort(int low, int high) {
		int i = low, j = high;
		// TODO menentukan pivot yaitu data tengah dari array
		String[] pivotArr=arr.get(low+(high-low)/2).split(" ");
		int pivot =Integer.parseInt(pivotArr[1]);
			// Dibagi menjadi 2 bagian (kiri pivot & kanan pivot)
		
		while (i <= j) {
			// Jika nilai i sekarang lebih besar dari pivot, maka i
			// menjadi setelahnya dari bagian kiri
			String[] tempi=arr.get(i).split(" ");
			String[] tempj=arr.get(j).split(" ");
			while (Integer.parseInt(tempi[1]) > pivot) {
				i++;// TODO increment i.
				tempi=arr.get(i).split(" ");
			}
			while (Integer.parseInt(tempj[1]) < pivot) {
				j--;// TODO decrement j.
				tempj=arr.get(j).split(" ");
			}
			while (tempi[0].compareTo(pivotArr[0])<0&&Integer.parseInt(tempi[1]) == pivot) {
				i++;// TODO increment i
				tempi=arr.get(i).split(" ");
			}
			while (tempj[0].compareTo(pivotArr[0])>0&&Integer.parseInt(tempj[1]) == pivot) {
				j--;// TODO decrement j.
				tempj=arr.get(j).split(" ");
			}
			
			if (i <= j) {
				String temp2=arr.get(i);
				arr.set(i,arr.get(j));
				arr.set(j,temp2);// TODO swap nilai dari i dan j
				i++;// TODO increment i
				j--;// TODO decrement j
			}
		}
		// Rekursif
		if (low < j){
			quicksort(low,j);
		}
		// TODO jalankan method ini dari low hingga j
		if (i < high){
			quicksort(i,high);
			// TODO jalankan method ini dari i hingga high
		}
	}
}