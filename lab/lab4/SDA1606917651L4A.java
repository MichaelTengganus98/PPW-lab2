import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class SDA1606917651L4A{
	
	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		int buffer=Integer.parseInt(in.readLine());
		int[] arr= new int[buffer];
		String[] strdex= in.readLine().split(" ");
		for (int i=0;i<strdex.length;i++){
			arr[i]=Integer.parseInt(strdex[i]);
		}
		int[] arrsorted=quicksort(arr,0,(arr.length-1));
		for (int i=0;i<arrsorted.length;i++){
			System.out.print(arrsorted[i]+" ");
		}
	}

	public static int[] quicksort(int[] arr, int low, int high) {
		int i = low, j = high;
		// TODO menentukan pivot yaitu data tengah dari array
		int pivot =arr[low+(high-low)/2];
			// Dibagi menjadi 2 bagian (kiri pivot & kanan pivot)
		
		while (i <= j) {
			// Jika nilai i sekarang lebih kecil dari pivot, maka i
			// menjadi setelahnya dari bagian kiri
			while (arr[i] > pivot) {
				i++;// TODO increment i.
			}
			// Jika nilai j sekarang lebih besar dari pivot, maka j
			// menjadi setelahnya dari bagian kanan
			while (arr[j] < pivot) {
				j--;// TODO decrement j.
			}
			if (i <= j) {
				int temp=arr[i];
				arr[i]=arr[j];
				arr[j]=temp;// TODO swap nilai dari i dan j
				i++;// TODO increment i
				j--;// TODO decrement j
			}
		}
		// Rekursif
		if (low < j){
			quicksort(arr,low,j);
		}
		// TODO jalankan method ini dari low hingga j
		if (i < high){
			quicksort(arr,i,high);
			// TODO jalankan method ini dari i hingga high
		}
		return arr;
	}
}