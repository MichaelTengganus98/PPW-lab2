import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Deque;
import java.util.ArrayDeque;
import java.util.TreeSet;
import java.util.Iterator;

public class SDA1718L2B{
public static Deque<String> garasi=new ArrayDeque<>();
public static TreeSet<String> mogok = new TreeSet<String>();

	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String str=in.readLine();
		
		while(str != null){
			String[] strdex = str.split(" ");
		
			//PRINT
			if (strdex[0].equals("MASUK")){
				masuk(strdex[1],strdex[2]);
			}
			else if (strdex[0].equals("KELUARKAN")){
				keluarkan(strdex[1]);
			}
			else if (strdex[0].equals("MOGOK")){
				mogok(strdex[1]);
			}
			else if (strdex[0].equals("SERVIS")){
				servis(strdex[1]);
			}
			str = in.readLine();
		}
	}

	//--------------------METHOD--------

	/**
	 * Masuk Method
	 * MASUK <JENIS MOBIL> <BARAT/TIMUR>
	 */
	public static void masuk(String mobil,String arah){
		if (!garasi.contains(mobil)){
			if (arah.equals("BARAT")){
				garasi.offerFirst(mobil);
				System.out.println(mobil+" masuk melalui pintu "+arah);
			}
			else if(arah.equals("TIMUR")){
				garasi.offerLast(mobil);
				System.out.println(mobil+" masuk melalui pintu "+arah);
			}
		}
	}
	
	/**
	 * Keluarkan Method
	 * KELUARKAN <JENIS MOBIL>
	 * 
	 * west/east for checking west/east broken car
	 * westcar/eastcar for the nearest west/east car
	 * v for car status in iter(have been iterate or not)
	 * trapped for the chance for the car to get out
	 * 
	 */
	public static void keluarkan(String mobil){
		boolean west=false;
		boolean east=false;
		boolean v=false;
		boolean trapped=false;
		int x=1;
		int urutan=0;
		String westcar="";
		String eastcar="";
		if(!garasi.contains(mobil)){
			System.out.println(mobil+" tidak ada di garasi");
		}
		else if(mogok.contains(mobil)){
			System.out.println("Mobil "+mobil+" sedang mogok");
		}
		else {
			for(Iterator<String> itr = garasi.iterator();itr.hasNext();)  {
				String iteritem=itr.next();
				if(iteritem.equals(mobil)){//untuk kananmoil
					v=true;
					urutan=x;
				}
				if(mogok.contains(iteritem)){
					if(v==false){
						west=true;
						westcar=iteritem;
					}
					else if(v==true&&east==false){
						east=true;
						eastcar=iteritem;
					}
				}
				x+=1;
			}
			if(west&&east){
				trapped=true;
			}
			if(trapped){
				System.out.println(mobil+" tidak bisa keluar, mobil "+ westcar+" dan " +eastcar+" sedang mogok");
			}
			else if(!west&&!east){
				if ((x/2-urutan+0.5)<0){
					System.out.println(mobil+" keluar melalui pintu BARAT"+" A");
					garasi.remove(mobil);
				}
				else{
					System.out.println(mobil+" keluar melalui pintu TIMUR"+" B");
					garasi.remove(mobil);
				}
			}
			else if(west==false){
				System.out.println(mobil+" keluar melalui pintu BARAT"+" C");
				garasi.remove(mobil);
			}
			else if(east==false){
				System.out.println(mobil+" keluar melalui pintu TIMUR"+" D");
				garasi.remove(mobil);
			}
		}
	}
	
	/**
	 * Mogok Method
	 */
	public static void mogok(String mobil){
		if (garasi.contains(mobil)){
			mogok.add(mobil);
		}
	}
	
	/**
	 * Servis Method
	 */
	public static void servis(String mobil){
		mogok.remove(mobil);
	}
}