import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Printin.id
 */
public class SDA1718L2A{
public static TreeMap<String,Integer> urutan= new TreeMap<String,Integer>();
public static TreeSet<String> done = new TreeSet<String>();

	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String str=in.readLine();
		
		while(str != null){
			String[] strdex = str.split(" ");
		
			//PRINT
			if (strdex[0].equals("PRINT")){
				print();
			}
			
			//STATUS
			else if(strdex[0].equals("STATUS")){
				System.out.println(status(strdex[1]));
			}

			//SUBMIT
			else if(strdex[1].equals("SUBMIT")){
				if(strdex[0].substring(0,1).equals("1") && strdex[0].equals(strdex[0].toUpperCase())&& Integer.parseInt(strdex[2])<1000){
					if(Integer.parseInt(strdex[2])>10){
						System.out.println("Jumlah halaman submisi "+strdex[0]+" terlalu banyak");
					}
					else if(urutan.containsKey(strdex[0])){
						System.out.println("Harap tunggu hingga submisi sebelumnya selesai diproses");
					}
					else{
						submit(strdex[0],Integer.parseInt(strdex[2]));
					}	
				}
			}
			
			//CANCEL
			else if(strdex[1].equals("CANCEL")){
				System.out.println(cancel(strdex[0]));
			}
			else{
			    continue;
            }
			str = in.readLine();
		}
	}
	
	//------------------METHOD-------------
	
	/**
	 * Print Method
	 * PRINT
	 * Printing 10 at a time
	 */
	public static void print(){
		int x=10;	//jumlah print
		while (x>0){
			if(urutan.isEmpty()&&x==10){
				System.out.println("Antrean kosong");
				x=0;
			}
			
			else if(!urutan.isEmpty()){
				int jumlah=urutan.get(urutan.firstKey());
				if (jumlah<=x){
					System.out.println("Submisi "+urutan.firstKey()+" telah dicetak sebanyak "+jumlah+" halaman");
					x-=jumlah;
					done.add(urutan.firstKey());
					urutan.remove(urutan.firstKey());
				}
				else if(x==10&jumlah>10){
					System.out.println("Submisi "+urutan.firstKey()+" telah dicetak sebanyak "+10+" halaman");
					urutan.put(urutan.firstKey(),jumlah-10);
					x=0;
				}
				else{
					x=0;
				}
			}
			else{
				x=0;
			}
		}
	}
	
	/**
	 * Submit Method
	 * <NPM SISWA> SUBMIT <JUMLAH HALAMAN>
	 * Submiting NPM and Halaman
	 */
	public static void submit(String npm, int halaman){
		if(!urutan.containsKey(npm)){
			urutan.put(npm,halaman);
			done.remove(npm);
			System.out.println("Submisi "+npm+" telah diterima");
		}
		else{
			Object value = urutan.get(npm);
		   	System.out.println("Key : " + npm +" value :"+ value);
		}
	}

	/**
	 * Status Method
	 * STATUS <NPM SISWA>
	 */
	public static String status(String npm){
		if(done.contains(npm)){
	        return "Submisi "+npm+" sudah diproses";
        }
        else if(urutan.containsKey(npm)){
            return "Submisi "+npm+" masih dalam antrean";
        }
        else{
            return npm+" tidak ada dalam sistem";
        }
	}
	
	/**
	 * Cancel Method
	 * <NPM SISWA> CANCEL
	 */
	public static String cancel(String npm){
		if(urutan.containsKey(npm)){
            urutan.remove(npm);
            return "Submisi "+npm+" dibatalkan";
        }
        else{
        	return npm+" tidak ada dalam antrean";
        }
    }
}