import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class SDA1606917651L3B{
	
	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		int buffer= Integer.parseInt(in.readLine());
		while(buffer>0){
			int testNumber=Integer.parseInt(in.readLine());
			System.out.println(rec(testNumber));
			buffer--;
		}
	}

	public static int rec(int N){
		if(N==0){
			return 0;
		}
		else if(N==1||N==2||N==3){
			return rec(N-1)+1;
		}
		else{
			boolean checker=true;
			boolean prime=false;
			int max=2;
			Integer min=2;
			while(checker){
				for(int i=2;i<N;i++) {
			        if(N%i==0){
			        	prime=true;
			        }
				}
				if(!prime){
					checker=false;
					return rec(N-1)+1;
				}
				else{
					int A=faktor(N,max);
					int B=N/A;
					if(B>=A){	
						checker=false;
						return rec(B)+1;
					}
					else{
						max++;
					}
				}
			}
			return 0;
		}
	}
	
	public static int faktor(int N,int max){
		int maxFak=2;
		for(int faktor=max;faktor<N;faktor++){
			int B=(int)N/faktor;
			if(B>=faktor){
				if(faktor>maxFak&&N%faktor==0){
					maxFak=faktor;
				}
			}
		}
		return maxFak;
	}
}