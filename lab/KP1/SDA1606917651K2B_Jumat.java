import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

import java.util.Set;
import java.util.HashSet;

/**
 * Michael Tengganus
 * NPM: 1606917651
 * Quiz Jumat-13 Oct 2017
 * Himpunan Hasil Perkalian
 */
public class SDA1606917651K2B_Jumat{
public static boolean bool;
	
	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		int buffer=Integer.parseInt(in.readLine());
		String str;
		for(int i=0;i<buffer;i++){
			str=in.readLine();
			String[] arrstr=str.split(" ");
			int[] arrint= new int[arrstr.length];
			for (int x=0;x<arrstr.length;x++){
				arrint[x]=Integer.parseInt(arrstr[x]);
			}
			str=in.readLine();
			check(arrint,Integer.parseInt(str));
		}
	}
	
	/**
	* Method check
	* digunakan untuk menge Print hasil powerset
	*/
	public static void check(int[] arr,int pivot){
		if(pivot==1){
			System.out.println("Tidak ada");
		}
		else if(rekursif(arr,pivot,0)){

			System.out.println("Ada");
		}
		else{
			System.out.println("Tidak ada");
		}
	}
	
	/**
	* Method Rekursif
	* digunakan untuk mengecheck Himpunan​ ​Hasil​ ​Perkalian
	*/
	public static boolean rekursif(int[] arr, int pivot,int i){
		if(pivot==1&&i==arr.length){
			return true;
		}
		else if(i<arr.length-1){
			if(pivot%arr[i]==0){
				pivot=pivot/arr[i];
				i++;
				return rekursif(arr,pivot,i);
			}
			else{
				i++;
				return rekursif(arr,pivot,i);
			}
		}
		else if(i==arr.length&&pivot!=1){
			return false;
		}
		else{
			i++;
			return rekursif(arr,pivot,i);
		}
	}	
}