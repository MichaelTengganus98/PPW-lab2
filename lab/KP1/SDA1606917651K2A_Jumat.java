
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.IOException;

import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.Stack;
import java.util.Queue;
import java.util.Map;
import java.util.Iterator;
import java.util.LinkedList;

// TODO: Ganti nama Main class sesuai soal, tambahkan variable yang dibutuhkan untuk permasalahan (Struktur Data, dll)
public class SDA1606917651K2A_Jumat
{
public static Stack<String> stack=new Stack<String>();
public static Queue<String> que=new LinkedList<String>();
public static TreeMap<String,Integer> map=new TreeMap<String,Integer>();

	public static void main(String[] args) throws IOException
	{
		// BufferedReader, untuk membaca input
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		
		// PrintWriter, untuk output. Penggunaannya seperti System.out
		PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(System.out)));
		
		// Baca input baris pertama: banyak 
		int inputCount = Integer.parseInt(in.readLine());
		
		// Loop untuk setiap baris input
		for (int i = 0; i < inputCount; i++) {
			// Baca input, tokenize dengan spasi
			String inp = in.readLine();
			StringTokenizer st = new StringTokenizer(inp);
			
			// Token pertama: jenis perintah
			String command = st.nextToken();
			
			if (command.equals("ANTRI")) {
				String nama = st.nextToken();
				int saldo = Integer.parseInt(st.nextToken());
				antri(nama, saldo);
			}
			else if (command.equals("PROSES")) {
				proses();
			}
			else if (command.equals("PENARIKAN")) {
				String nama = st.nextToken();
				penarikan(nama);
			}
		}
		
		// Close output
		out.close();
	}
	
	/**
	* Method untuk menghandle perintah "ANTRI"
	*/
	public static void antri(String nama, int saldo)
	{
		map.put(nama, saldo);
		que.add(nama);

		System.out.println(nama+" masuk ke dalam antrian");
		// TODO: Implementasikan untuk perintah "ANTRI"
	}
	
	/**
	* Method untuk menghandle perintah "PROSES"
	*/
	public static void proses()
	{
		if(que.isEmpty()){
			System.out.println("Antrian kosong");	
		}
		else{
			String nama=que.peek();
			int saldo=map.get(que.peek());
			int min;
			if(stack.isEmpty()){
				System.out.println(nama +" berhasil menabung sebesar "+ saldo);	
				stack.add(que.remove());
			}
			else{
				saldo=map.get(que.peek());
				min=map.get(stack.peek());
				Iterator iter = que.iterator();
				while (iter.hasNext()) {
					int temp=map.get(iter.next());
				    if(min>temp){
				    	min=temp;
				    }
				}
				int max=1000*min;
				if(saldo<=(max)){
					System.out.println(nama +" berhasil menabung sebesar "+ saldo);	
					stack.add(que.remove());
				}		
				else{
					System.out.println("Saldo "+ nama + " melebihi saldo maksimal: "+max);
					que.remove();
				}
			}
		}
		
		// TODO: Implementasikan untuk perintah "PROSES"
	}
	
	/**
	* Method untuk menghandle perintah "PENARIKAN"
	*/
	public static void penarikan(String nama)
	{
		if(stack.isEmpty()){
			System.out.println(nama+ " tidak berhasil melakukan penarikan");
		}
		else{
			if(stack.peek().equals(nama)){
				int saldo=map.get(stack.peek());
				System.out.println(nama+" berhasil melakukan penarikan sebesar " + saldo);
				map.remove(nama);
				stack.pop();
			}
			else{
				System.out.println(nama+ " tidak berhasil melakukan penarikan");
			}
		}
		
		// TODO: Implementasikan untuk perintah "PENARIKAN"
	}
}