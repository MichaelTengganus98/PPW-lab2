
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.IOException;

import java.util.StringTokenizer;

// TODO: Ganti nama Main class sesuai soal
public class TemplateK2A_Jumat_V1
{
	public static void main(String[] args) throws IOException
	{
		// BufferedReader, untuk membaca input
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		
		// PrintWriter, untuk output. Penggunaannya seperti System.out
		PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(System.out)));
		
		// Baca input baris pertama: banyak 
		int inputCount = Integer.parseInt(in.readLine());
		
		// Inisialisasi objek untuk sistem
		SistemBankUnik sbu = new SistemBankUnik();
		
		// Loop untuk setiap baris input
		for (int i = 0; i < inputCount; i++) {
			// Baca input, tokenize dengan spasi
			String inp = in.readLine();
			StringTokenizer st = new StringTokenizer(inp);
			
			// Token pertama: jenis perintah
			String command = st.nextToken();
			
			if (command.equals("ANTRI")) {
				String nama = st.nextToken();
				int saldo = Integer.parseInt(st.nextToken());
				sbu.antri(nama, saldo);
			}
			else if (command.equals("PROSES")) {
				sbu.proses();
			}
			else if (command.equals("PENARIKAN")) {
				String nama = st.nextToken();
				sbu.penarikan(nama);
			}
		}
		
		// Close output
		out.close();
	}
}

/**
* Class yang merepresentasikan Sistem Bank Unik
*/
class SistemBankUnik
{
	// TODO: Tambahkan instance variable yang dibutuhkan di sini
	
	/**
	* Constructor kosong
	*/
	public SistemBankUnik()
	{
		// TODO: Lengkapi constructor ini
	}
	
		/**
	* Method untuk menghandle perintah "ANTRI"
	*/
	public void antri(String nama, int saldo)
	{
		// TODO: Implementasikan untuk perintah "ANTRI"
	}
	
	/**
	* Method untuk menghandle perintah "PROSES"
	*/
	public void proses()
	{
		// TODO: Implementasikan untuk perintah "PROSES"
	}
	
	/**
	* Method untuk menghandle perintah "PENARIKAN"
	*/
	public void penarikan(String nama)
	{
		// TODO: Implementasikan untuk perintah "PENARIKAN"
	}
}