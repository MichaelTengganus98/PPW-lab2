import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.Collections;
import java.util.PriorityQueue;

public class SDA1606917651K3B_Jumat {

	public static void main(String[]args)throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		int count=Integer.parseInt(in.readLine());
		PriorityQueue<Integer> heap=new PriorityQueue<Integer>(count, Collections.reverseOrder());
		String[] str=in.readLine().split(" ");
		int cost=0;
		for(int i=0;i<count;i++){
			heap.add(Integer.parseInt(str[i]));
		}
		
		while(!heap.isEmpty() && heap.size()>1){
			int a=heap.poll();
			int b=heap.poll();
			int diff=a-b;
			if(diff == 0){
				continue;
			}
			else{
				cost+=diff;
				heap.add(diff);
			}
		}	
		System.out.println(cost);
	}
}