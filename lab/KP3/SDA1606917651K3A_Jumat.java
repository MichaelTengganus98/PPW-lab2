import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;
import java.util.List;
import java.util.LinkedList;

/**
 * TEMPLATE KUIS 3 A JUMAT
 * ANDA BOLEH MEMODIFIKASI TEMPLATE INI SESUKA HATI ANDA
 * ANDA JUGA BOLEH TIDAK MENGGUNAKAN TEMPLATE INI
 * ANDA BISA MENGERJAKAN PADA BAGIAN TODO ATAU BAGIAN YANG LAINNYA
 *
 * SEMANGAT :)
 */
public class SDA1606917651K3A_Jumat {

    public static void main(String[] args){
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(System.in));
            int maxOrangDiBus = Integer.parseInt(reader.readLine());
            Bus bus = new Bus(maxOrangDiBus);
            int m = Integer.parseInt(reader.readLine());
            for(int i = 0 ; i < m ; i++){
                StringTokenizer input = new StringTokenizer(reader.readLine());
                String command = input.nextToken();
                if(command.equals("NAIK")){
                    String nama = input.nextToken();
                    int umur = Integer.parseInt(input.nextToken());
                    System.out.println(bus.naik(new Penumpang(nama, umur, -1)));
                }else if(command.equals("TURUN")){
                    String nama = input.nextToken();
                    int umur = Integer.parseInt(input.nextToken());
                    System.out.println(bus.turun(new Penumpang(nama,umur,-1)));
                }else if(command.equals("MELIHAT")){
                    bus.melihat(Integer.parseInt(input.nextToken()), Integer.parseInt(input.nextToken()));
                }
            }
        } catch (IOException e) {
        } finally {
            if(reader != null){
                try {
                    reader.close();
                } catch (IOException e) {
                }
            }
        }


    }

    static class Bus{

        private int maxOrang;
        private BSTOrang bstOrang;

        public Bus(int maxOrang) {
            this.maxOrang = maxOrang;
            bstOrang = new BSTOrang();
        }

        public String naik(Penumpang penumpang){
            if(bstOrang.getSize() == maxOrang){
                bstOrang.removeLast();
            }

            penumpang.setWaktuMenaikiBus(bstOrang.getSize() + 1);

            String output = penumpang.getNama() + " dengan umur " + penumpang.getUmur() + " tahun menaiki bus";

            Penumpang result = bstOrang.get(penumpang).penumpang;
            if(result != null){
                output = result.getNama() + " dengan umur " + result.getUmur() + " tahun yang menaiki bus pada waktu ke-" + result.getWaktuMenaikiBus() + " dikeluarkan dari bus";
                result.setWaktuMenaikiBus(penumpang.getWaktuMenaikiBus());
            }else{
                bstOrang.add(penumpang);
            }
            return output;
        }

        public String turun(Penumpang penumpang){
            boolean isSuccess = bstOrang.remove(penumpang);
            if(isSuccess)
                return penumpang.getNama() + " dengan umur " + penumpang.getUmur() + " tahun yang menaiki bus pada waktu ke-"+penumpang.getWaktuMenaikiBus()+" turun dari bus";
        	return null;
        }

        public String melihat(int start, int finish){
            //TODO
        	List<Penumpang> list=bstOrang.inOrderAscending();
        	for(int i=start-1;i<finish;i++){
        		System.out.println(list.get(i).getNama()+" dengan umur "+list.get(i).getUmur());
        	}
        	return null;
        }

    }

    static class BSTOrang{
    	

    	class Node{
            Penumpang penumpang;
            Node left;
            Node right;
            Node parent;
            
    		public Node(Penumpang penumpang, Node left, Node right, Node parent){
    			this.penumpang = penumpang;
    			this.left = left;
    			this.right = right;
    			this.parent = parent;
    		}
    	}
    	
        private Node root;

        public BSTOrang(){
        }

    	public boolean isEmpty(){
    		
    		return root == null;
    		
    	}
    	
        public int getSize() {
            //TODO
            return -1;
        }
        
        public Penumpang removeLast(){
            //TODO
            return null;
        }

    	public void add(Penumpang penumpang){
    		//initiate BTS
    		if(root == null){
    			root=new Node(penumpang,null,null,null); //Node with penumpang only
    		} else {
    			Node current = root;
    			while(current != null){
    				Penumpang currElem = current.penumpang;
    				//Node < Current
    				if(penumpang.compareTo(currElem) < 0){
    					if(current.left==null){
    						Node temp=new Node(penumpang,null,null,current);
    						current.left=temp;
    						current=temp;
    					}
    					else{
    						//move node
    						current=current.left;
    					}
    				}
    				//Node > Current
    				else if(penumpang.compareTo(currElem) > 0){
    					if(current.right==null){
    						Node temp=new Node(penumpang,null,null,current);
    						current.right=temp;
    						current=temp;
    					}
    					else{
    						//move node
    						current=current.right;
    					}
    				}
    				else{
    					break;
    				}
    			}
    		}
    	}
    	
    	private Node get(Penumpang penumpang){
    		
    		Node res = null;
    		
    		if(root != null){
    			//creating temp root
    			Node current = root;
    			boolean found = false;
    			while(!found && current != null){
    				Penumpang currElem = current.penumpang;
    				//Node penumpang < Current penumpang
    				if(penumpang.compareTo(currElem) < 0){
    					current=current.left;
    				}
    				//Node penumpang > Current penumpang
    				else if(penumpang.compareTo(currElem) > 0){
    					current=current.right;
    				}
    				//Node penumpang = Current penumpang
    				else {
    					found=true;
    					res=current;
    				}
    			}
    		}
    		
    		return res;
    		
    	}

    	public boolean remove(Penumpang penumpang){
    		if(get(penumpang)!=null){
    			root=remove(penumpang,root);
    			return true;
    		}
    		else{
    			return false;
    		}
    	}
    	
    	public Node removeMin(Node node){
    		if(node==null){
    			return node;
    		}
    		if(node.left!=null){
    			node.left=removeMin(node.left);
    			return node;
    		}
    		else{
    			return node.right;
    		}
    	}
    	
    	public Node remove(Penumpang penumpang,Node node){
    		//node null
    		if(node==null){
    			return node;
    		}
    		//node < penumpang
    		if(penumpang.compareTo(node.penumpang)<0){
    			node.left=remove(penumpang,node.left);
    		}
    		//node > penumpang
    		else if(penumpang.compareTo(node.penumpang)>0){
    			node.right=remove(penumpang,node.right);
    		}
    		//2 child
    		else if(node.left!=null && node.right!=null){
    			//replace node with inorder successor
    			node.penumpang=minNode(node.right).penumpang;
    			//remove inorder successor
    			node.right=removeMin(node.right);
    		}
    		//1 child
    		else{
    			node=(node.left!=null)?node.left:node.right;
    		}
    		return node;
    	}
    		

    	public Penumpang min(){
    		
    		Penumpang res = null;
    		Node minNode = minNode(root);
    		
    		if(minNode != null){
    			
    			res = minNode.penumpang;
    			
    		}
    		
    		return res;
    		
    	}

    	private Node minNode(Node node){
    		
    		Node res = null;
    		if(node != null){
    			//temp node
    			Node current = node;
    			//getting the smallest penumpangent
    			while (current.left!=null){
    				current=current.left;
    			}
    			res=current;
    		}
    		
    		return res;
    		
    	}

    	public Penumpang max(){
    		
    		Penumpang res = null;
    		Node maxNode = maxNode(root);
    		
    		if(maxNode != null){
    			
    			res = maxNode.penumpang;
    			
    		}
    		
    		return res;
    		
    	}

    	private Node maxNode(Node node){
    		
    		Node res = null;
    		if(node != null){
    			//temp node
    			Node current = node;
    			//getting the highest penumpangent
    			while (current.right!=null){
    				current=current.right;
    			}
    			res=current;
    		}
    		
    		return res;
    	}
    	
    	public boolean contains(Penumpang penumpang){
    		if(get(penumpang)!=null){
    			return true;
    		}
    		else{
    			return false;
    		}
    	}
    	
    	public List<Penumpang> inOrderAscending(){
    		
    		List<Penumpang> list = new LinkedList<>(); // default menggunakan LinkedList, silahkan menggunakan List yang sesuai dengan Anda
    		return inOrderAscending(root,list);
    		
    	}
    	
    
    	private List<Penumpang> inOrderAscending(Node node, List<Penumpang> list){
    		if(node==null){
    			return list;
    		}
    		if(node.left!=null){
    			//getting left child
    			inOrderAscending(node.left,list);
    		}
    		//adding node.penumpang
    		list.add(node.penumpang);
    		if(node.right!=null){
    			//getting right child
    			inOrderAscending(node.right,list);
    		}
    		return list;
    		
    	}

    }

    static class Penumpang implements Comparable<Penumpang>{

        private String nama;
        private Integer umur;
        private Integer waktuMenaikiBus;

        public Penumpang(String nama, int umur, int waktuMenaikiBus) {
            this.nama = nama;
            this.umur = umur;
            this.waktuMenaikiBus = waktuMenaikiBus;
        }

        public Penumpang() {
        }

        public String getNama() {
            return nama;
        }

        public void setNama(String nama) {
            this.nama = nama;
        }

        public int getUmur() {
            return umur;
        }

        public void setUmur(int umur) {
            this.umur = umur;
        }

        public int getWaktuMenaikiBus() {
            return waktuMenaikiBus;
        }

        public void setWaktuMenaikiBus(int waktuMenaikiBus) {
            this.waktuMenaikiBus = waktuMenaikiBus;
        }

        public int compareTo(Penumpang other){
    		int temp=this.umur.compareTo(other.getUmur());
    		if(temp!=0){
    			return temp;
    		}
    		temp=this.waktuMenaikiBus.compareTo(other.getWaktuMenaikiBus());
    		if(temp!=0){
    			return temp;
    		}
    				
    		return this.nama.compareTo(other.getNama());
    	}
    }
}
