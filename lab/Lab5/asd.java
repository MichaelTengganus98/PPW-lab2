import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class asd{

	public static void main(String[] args) throws IOException {
		BSTree<String> bst=new BSTree<String>();
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String str=in.readLine();
		while(str!=null){
			String[] strdex = str.split(";");
			if(strdex[0].equals("REGISTER")){
				if(bst.add(strdex[1],strdex[2])){
					System.out.println(strdex[1]+":"+strdex[2]+" berhasil ditambahkan");
				}
				else{
					System.out.println(strdex[1]+ " sudah terdaftar di dalam sistem");
				}
			}
			else if(strdex[0].equals("RESIGN")){
				if(bst.remove(strdex[1])){
					System.out.println(strdex[1]+ " mengundurkan diri");
				}
				else{
					System.out.println(strdex[1]+ " tidak ditemukan di dalam sistem");
				}
			}
			else if(strdex[0].equals("RETEST")){
				if(bst.remove(strdex[1],strdex[2])){
					bst.add(strdex[1], strdex[2]);
					System.out.println(strdex[1]+":"+strdex[2]+" perubahan nilai berhasil");	
				}
				else{
					System.out.println(strdex[1]+" tidak ditemukan di dalam sistem");
				}
				
			}
			else if(strdex[0].equals("SMARTEST")){
				ArrayList<ArrayList<String>> list=bst.inOrderDescending();
				if(list.size()!=0){
					int i=0;
					System.out.print(list.get(i).get(1));
					boolean check=false;
					if(i<list.size()-1){
						check=(list.get(i).get(0).equals(list.get(i+1).get(0)));
					}
					while(check){
						i++;
						System.out.print(", "+list.get(i).get(1));
						if(i<list.size()-1){
							check=(list.get(i).get(0).equals(list.get(i+1).get(0)));	
						}
					}
					System.out.println(" : "+list.get(i).get(0));
				}
				else{
					System.out.println("Tidak ada siswa yang terdaftar dalam sistem");
				}
			}
			else if(strdex[0].equals("RANKING")){
				ArrayList<ArrayList<String>> list=bst.inOrderDescending();
				int counter=1;
				if(list.size()!=0){
					for(int i=0;i<list.size();i++){
						System.out.print(counter+". "+list.get(i).get(1));
						boolean check=false;
						if(i<list.size()-1){
							check=(list.get(i).get(0).equals(list.get(i+1).get(0)));
						}
						while(check){
							i++;
							System.out.print(", "+list.get(i).get(1));
							if(i<list.size()-1){
								check=(list.get(i).get(0).equals(list.get(i+1).get(0)));	
							}
							else{
								check=false;
							}
						}
						System.out.println(" : "+list.get(i).get(0));
						counter++;
					}
				}else{
					System.out.println("Tidak ada siswa  yang terdaftar dalam sistem");
				}	
			}
			str=in.readLine();
		}
	}
}

class Human{
	private static
}

class BSTree<E extends Comparable<? super E>> {
	
	/**
	  *
	  * Kelas yang merepresentasikan node pada tree
	  * @author Jahns Christian Albert
	  *
	*/
	private static class Node<E> {
		
		E elem;
		Node<E> left;
		Node<E> right;
		Node<E> parent;
		
		/**
		 *
		 * Constructor
		 * @param elemen pada node
		 * @param node kiri
		 * @param node kanan
		 * @param node parent
		 *
		*/
		public Node(E elem, Node<E> left, Node<E> right, Node<E> parent){
			
			this.elem = elem;
			this.left = left;
			this.right = right;
			this.parent = parent;
			
		}
		
	}
	
	private Node<E> root;
	
	/**
	  *
	  * Constructor Kelas Binary Search Tree
	  *
	*/
	public BSTree(){
		
		root = null;
		
	}
	
	/**
	  *
	  * Mengetahui apakah tree kosong atau tidak
	  * @return true jika kosong, false jika sebaliknya
	  *
	*/
	public boolean isEmpty(){
		
		return root == null;
		
	}
	
	/**
	  *
	  * Menambahkan objek ke dalam tree
	  * @param elemen yang ingin ditambahkan
	  * @return true jika elemen berhasil ditambahkan, false jika elemen sudah terdapat pada tree
	  *
	*/
	public boolean add(E elem){
		boolean res = false;
		//initiate BTS
		if(root == null){
			root=new Node<>(elem,null,null,null); //Node with elem only
			res=true;
		} else {
			Node<E> current = root;
			while(current != null){
				E currElem = current.elem;
				//Node < Current
				if(elem.compareTo(currElem) < 0){
					if(current.left==null){
						Node<E> temp=new Node<>(elem,null,null,current);
						current.left=temp;
						current=temp;
						res=true;
					}
					else{
						//move node
						current=current.left;
					}
				}
				//Node > Current
				else if(elem.compareTo(currElem) > 0){
					if(current.right==null){
						Node<E> temp=new Node<>(elem,null,null,current);
						current.right=temp;
						current=temp;
						res=true;
					}
					else{
						//move node
						current=current.right;
					}
				}
				else{
					break;
				}
			}
		}
		return res;
	}
	
	/**
	  *
	  * Mendapatkan node dengan elemen tertentu
	  * @param elemen yang ingin dicari nodenya
	  * @return node dari elemen pada parameter, null jika tidak ditemukan
	  *
	*/
	private Node<E> find(E elem){
		
		Node<E> res = null;
		
		if(root != null){
			//creating temp root
			Node<E> current = root;
			boolean found = false;
			while(!found && current != null){
				E currElem = current.elem;
				//Node elem < Current elem
				if(elem.compareTo(currElem) < 0){
					current=current.left;
				}
				//Node elem > Current elem
				else if(elem.compareTo(currElem) > 0){
					current=current.right;
				}
				//Node elem = Current elem
				else {
					found=true;
					res=current;
				}
			}
		}
		
		return res;
		
	}
	
	/**
	 *
	 * Menghapus objek dari tree, menggunakan successor inorder untuk menghapus elemen yang memiliki left node dan right node
	 * Manfaatkan method minNode(Node<E> node) untuk mencari successor inorder
	 * @param elemen yang ingin dihapus
	 * @return true jika elemen ditemukan dan berhasil dihapus, false jika elemen tidak ditemukan
	 *
	*/
	public boolean remove(E elem){
		if(find(elem)!=null){
			root=remove(elem,root);
			return true;
		}
		else{
			return false;
		}
	}
	
	/**
	 * Menghapus inorder successor dari child kanan Tree
	 * @param Node dari Node.right
	 * @return Node inroder successornya
	 */
	public Node<E> removeMin(Node<E> node){
		if(node==null){
			return node;
		}
		if(node.left!=null){
			node.left=removeMin(node.left);
			return node;
		}
		else{
			return node.right;
		}
	}
	
	public Node<E> remove(E elem,Node<E> node){
		//node null
		if(node==null){
			return node;
		}
		//node < elem
		if(elem.compareTo(node.elem)<0){
			node.left=remove(elem,node.left);
		}
		//node > elem
		else if(elem.compareTo(node.elem)>0){
			node.right=remove(elem,node.right);
		}
		//2 child
		else if(node.left!=null && node.right!=null){
			//replace node with inorder successor
			node.elem=minNode(node.right).elem;
			//remove inorder successor
			node.right=removeMin(node.right);
		}
		//1 child
		else{
			node=(node.left!=null)?node.left:node.right;
		}
		return node;
	}
		
	/**
	 *
	 * Mencari elemen dengan nilai paling kecil pada tree
	 * @return elemen dengan nilai paling kecil pada tree
	 *
	*/
	public E min(){
		
		E res = null;
		Node<E> minNode = minNode(root);
		
		if(minNode != null){
			
			res = minNode.elem;
			
		}
		
		return res;
		
	}
	
	/**
	  *
	  * Method untuk mengembalikan node dengan elemen terkecil pada suatu subtree
	  * Hint : Manfaatkan struktur dari binary search tree
	  * @param node root dari subtree yang ingin dicari elemen terbesarnya
	  * @return node dengan elemen terkecil dari subtree yang diinginkan
	  *
	*/
	private Node<E> minNode(Node<E> node){
		
		Node<E> res = null;
		if(node != null){
			//temp node
			Node<E> current = node;
			//getting the smallest element
			while (current.left!=null){
				current=current.left;
			}
			res=current;
		}
		
		return res;
		
	}
	
	/**
	 *
	 * Mencari elemen dengan nilai paling besar pada tree
	 * @return elemen dengan nilai paling besar pada tree
	 *
	*/
	public E max(){
		
		E res = null;
		Node<E> maxNode = maxNode(root);
		
		if(maxNode != null){
			
			res = maxNode.elem;
			
		}
		
		return res;
		
	}
	
	/**
	  *
	  * Method untuk mengembalikan node dengan elemen terbesar pada suatu subtree
	  * Hint : Manfaatkan struktur dari binary search tree
	  * @param node root dari subtree yang ingin dicari elemen terbesarnya
	  * @return node dengan elemen terbesar dari subtree yang diinginkan
	  *
	*/
	private Node<E> maxNode(Node<E> node){
		
		Node<E> res = null;
		if(node != null){
			//temp node
			Node<E> current = node;
			//getting the highest element
			while (current.right!=null){
				current=current.right;
			}
			res=current;
		}
		
		return res;
		
	}
	
	/**
	  *
	  * Mengetahui apakah sebuah objek sudah terdapat pada tree
	  * Asumsikan jika elem.compareTo(otherElem) == 0, maka elem dan otherElem merupakan objek yang sama
	  * Hint : Manfaatkan method find
	  * @param elemen yang ingin diketahui keberadaannya dalam tree
	  * @return true jika elemen ditemukan, false jika sebaliknya
	  *
	*/
	public boolean contains(E elem){
		if(find(elem)!=null){
			return true;
		}
		else{
			return false;
		}
	}
	
	/**
	  * Mengembalikan tree dalam bentuk pre-order
	  * @return tree dalam bentuk pre-order sebagai list of E
	*/
	public List<E> preOrder(){
		
		List<E> list = new LinkedList<>(); // default menggunakan LinkedList, silahkan menggunakan List yang sesuai dengan Anda
		return preOrder(root,list);
		
	}
	
	/**
	  *
	  * Method helper dari preOrder()
	  * @param node pointer
	  * @param list sebagai akumulator
	  * @return kumpulan elemen dari subtree yang rootnya adalah node parameter dengan urutan pre-order
	  *
	*/
	private List<E> preOrder(Node<E> node, List<E> list){
		if(node==null){
			return list;
		}
		
		//adding node.elem
		list.add(node.elem);
		if(node.left!=null){
			//getting left child
			preOrder(node.left,list);
		}
		if(node.right!=null){
			//getting right child
			preOrder(node.right,list);
		}
		return list;
		
	}
	
	/**
	  * Mengembalikan tree dalam bentuk post-order
	  * @return tree dalam bentuk post-order sebagai list of E
	*/
	public List<E> postOrder(){
		
		List<E> list = new LinkedList<>(); // default menggunakan LinkedList, silahkan menggunakan List yang sesuai dengan Anda
		return postOrder(root,list);

	}
	
	/**
	  *
	  * Method helper dari postOrder()
	  * @param node pointer
	  * @param list sebagai akumulator
	  * @return kumpulan elemen dari subtree yang rootnya adalah node parameter dengan urutan post-order
	  *
	*/
	private List<E> postOrder(Node<E> node, List<E> list){
		if(node==null){
			return list;
		}
		if(node.left!=null){
			//getting left child
			postOrder(node.left,list);
		}
		if(node.right!=null){
			//getting right child
			postOrder(node.right,list);
		}
		//adding node.elem
		list.add(node.elem);
		return list;
		
	}
	
	
	/**
	  * Mengembalikan tree dalam bentuk in-order secara ascending
	  * @return tree dalam bentuk in-order secara ascending sebagai list of E
	*/
	public List<E> inOrderAscending(){
		
		List<E> list = new LinkedList<>(); // default menggunakan LinkedList, silahkan menggunakan List yang sesuai dengan Anda
		return inOrderAscending(root,list);
		
	}
	
	/**
	  *
	  * Method helper dari inOrderAscending()
	  * @param node pointer
	  * @param list sebagai akumulator
	  * @return kumpulan elemen dari subtree yang rootnya adalah node parameter dengan urutan in-order secara ascending
	  *
	*/
	private List<E> inOrderAscending(Node<E> node, List<E> list){
		if(node==null){
			return list;
		}
		if(node.left!=null){
			//getting left child
			inOrderAscending(node.left,list);
		}
		//adding node.elem
		list.add(node.elem);
		if(node.right!=null){
			//getting right child
			inOrderAscending(node.right,list);
		}
		return list;
		
	}
	
	
	/**
	  * Mengembalikan tree dalam bentuk in-order secara descending
	  * @return tree dalam bentuk in-order secara descending sebagai list of E
	*/
	public List<E> inOrderDescending(){
		
		List<E> list = new LinkedList<>(); // default menggunakan LinkedList, silahkan menggunakan List yang sesuai dengan Anda
		return inOrderDescending(root,list);
		
	}
	
	/**
	  *
	  * Method helper dari inOrderDescending()
	  * @param node pointer
	  * @param list sebagai akumulator
	  * @return kumpulan elemen dari subtree yang rootnya adalah node parameter dengan urutan in-order descending
	  *
	*/
	private List<E> inOrderDescending(Node<E> node, List<E> list){
		if(node==null){
			 return list;
		}
		if(node.right!=null){
			//getting right child
			inOrderDescending(node.right,list);
		}
		//adding node.elem
		list.add(node.elem);
		if(node.left!=null){
			//getting left child
			inOrderDescending(node.left,list);
		}
		return list;
	}	
}