import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class SDA1606917651L5B{

	public static void main(String[] args) throws IOException {
		BSTree<String> bst=new BSTree<String>();
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String str=in.readLine();
		while(str!=null){
			String[] strdex = str.split(";");
			if(strdex[0].equals("REGISTER")){
				if(bst.add(strdex[1],strdex[2])){
					System.out.println(strdex[1]+":"+strdex[2]+" berhasil ditambahkan");
				}
				else{
					System.out.println(strdex[1]+ " sudah terdaftar di dalam sistem");
				}
			}
			else if(strdex[0].equals("RESIGN")){
				if(bst.remove(strdex[1])){
					System.out.println(strdex[1]+ " mengundurkan diri");
				}
				else{
					System.out.println(strdex[1]+ " tidak ditemukan di dalam sistem");
				}
			}
			/*else if(strdex[0].equals("RETEST")){
				if(bst.remove(strdex[1])){
					bst.add(strdex[1], strdex[2]);
					System.out.println(strdex[1]+":"+strdex[2]+" perubahan nilai berhasil");	
				}
				else{
					System.out.println(strdex[1]+" tidak ditemukan di dalam sistem");
				}
				
			}*/
			else if(strdex[0].equals("SMARTEST")){
				ArrayList<ArrayList<String>> list=bst.inOrderDescending();
				if(list.size()!=0){
					int i=0;
					System.out.print(list.get(i).get(1));
					boolean check=false;
					if(i<list.size()-1){
						check=(list.get(i).get(0).equals(list.get(i+1).get(0)));
					}
					while(check){
						i++;
						System.out.print(", "+list.get(i).get(1));
						if(i<list.size()-1){
							check=(list.get(i).get(0).equals(list.get(i+1).get(0)));	
						}
					}
					System.out.println(" : "+list.get(i).get(0));
				}
				else{
					System.out.println("Tidak ada siswa yang terdaftar dalam sistem");
				}
			}
			else if(strdex[0].equals("RANKING")){
				ArrayList<ArrayList<String>> list=bst.inOrderDescending();
				int counter=1;
				if(list.size()!=0){
					for(int i=0;i<list.size();i++){
						System.out.print(counter+". "+list.get(i).get(1));
						boolean check=false;
						if(i<list.size()-1){
							check=(list.get(i).get(0).equals(list.get(i+1).get(0)));
						}
						while(check){
							i++;
							System.out.print(", "+list.get(i).get(1));
							if(i<list.size()-1){
								check=(list.get(i).get(0).equals(list.get(i+1).get(0)));	
							}
							else{
								check=false;
							}
						}
						System.out.println(" : "+list.get(i).get(0));
						counter++;
					}
				}else{
					System.out.println("Tidak ada siswa  yang terdaftar dalam sistem");
				}	
			}
			str=in.readLine();
		}
	}
}
class BSTree<E extends Comparable<? super E>> {
	private ArrayList<E> arr=new ArrayList<>();
	/**
	  *
	  * Kelas yang merepresentasikan node pada tree
	  * @author Jahns Christian Albert
	  *
	*/
	private static class Node<E> {
		
		E elem;
		E value;
		Node<E> left;
		Node<E> right;
		Node<E> parent;
		
		/**
		 *
		 * Constructor
		 * @param elemen pada node
		 * @param node kiri
		 * @param node kanan
		 * @param node parent
		 *
		*/
		public Node(E elem,E value, Node<E> left, Node<E> right, Node<E> parent){
			this.elem = elem;
			this.value = value;
			this.left = left;
			this.right = right;
			this.parent = parent;
			
		}
		
	}
	
	private Node<E> root;
	
	/**
	  *
	  * Constructor Kelas Binary Search Tree
	  *
	*/
	public BSTree(){
		
		root = null;
		
	}
	
	/**
	  *
	  * Mengetahui apakah tree kosong atau tidak
	  * @return true jika kosong, false jika sebaliknya
	  *
	*/
	public boolean isEmpty(){
		
		return root == null;
		
	}
	
	/**
	  *
	  * Menambahkan objek ke dalam tree
	  * @param elemen yang ingin ditambahkan
	  * @return true jika elemen berhasil ditambahkan, false jika elemen sudah terdapat pada tree
	  *
	*/
	public boolean add(E elem,E value){
		boolean res = false;
		//initiate BTS
		if(root == null){
			root=new Node<>(elem,value,null,null,null); //Node with elem only
			res=true;
		} else if(arr.contains(elem)){
			res=false;
		} else {
			Node<E> current = root;
			while(current != null){
				E currElem = current.elem;
				E currValue = current.value;
				//Value < Current Value
				if(Integer.parseInt(String.valueOf(value))<Integer.parseInt(String.valueOf(currValue))){
					//System.out.println(value+" Value < Current Value "+currValue);
					if(current.left==null){
						Node<E> temp=new Node<>(elem,value,null,null,current);
						current.left=temp;
						current=temp;
						res=true;
					}
					else{
						//move node
						current=current.left;
					}
				}
				//Value > Current  Value
				else if(Integer.parseInt(String.valueOf(value))>Integer.parseInt(String.valueOf(currValue))){
					//System.out.println("Value > Current Value");
					if(current.right==null){
						Node<E> temp=new Node<>(elem,value,null,null,current);
						current.right=temp;
						current=temp;
						res=true;
					}
					else{
						//move node
						current=current.right;
					}
				}
				//Same Value
				else if(value.compareTo(currValue)==0){
					//System.out.println("Value = Current Value");
					if(elem.compareTo(currElem) > 0){
						//System.out.println("Elem < Current Elem");
						if(current.left==null){
							Node<E> temp=new Node<>(elem,value,null,null,current);
							current.left=temp;
							current=temp;
							res=true;
						}
						else{
							//move node
							current=current.left;
						}
					}
					//Node > Current
					else if(elem.compareTo(currElem) < 0){
						//System.out.println("Elem > Current Elem");
						if(current.right==null){
							Node<E> temp=new Node<>(elem,value,null,null,current);
							current.right=temp;
							current=temp;
							res=true;
						}
						else{
							//move node
							current=current.right;
						}
					}
					else{
						//System.out.println("break");
						break;
					}
				}
				else{
					break;
				}
			}
		}
		if(res){
			arr.add(elem);
		}
		return res;
	}
	
	public boolean findelem(E elem){
		Node<E> list=root;
		if(findelem(elem, list)!=null){
			return true;
		}
		else{
			return false;
		}
	}
	private Node<E> findelem(E elem,Node<E> node){
		if(node!=null){
			if(node.elem.compareTo(elem)==0){
				return node;
			}
			else{
				findelem(elem,node.left);
				findelem(elem,node.right);
			}
		}
		return null;
	}
	
	
	/**
	 *
	 * Menghapus objek dari tree, menggunakan successor inorder untuk menghapus elemen yang memiliki left node dan right node
	 * Manfaatkan method minNode(Node<E> node) untuk mencari successor inorder
	 * @param elemen yang ingin dihapus
	 * @return true jika elemen ditemukan dan berhasil dihapus, false jika elemen tidak ditemukan
	 *
	*/
	public boolean remove(E elem){
		Node<E> temp=root;
		temp=findelem(elem,temp);
		if(temp!=null){
			root=remove(temp,root);
			return true;
		}
		else{
			return false;
		}
	}
	
	/**
	 * Menghapus inorder successor dari child kanan Tree
	 * @param Node dari Node.right
	 * @return Node inroder successornya
	 */
	public Node<E> removeMin(Node<E> node){
		if(node==null){
			return node;
		}
		if(node.left!=null){
			node.left=removeMin(node.left);
			return node;
		}
		else{
			return node.right;
		}
	}
	
	public Node<E> remove(Node<E> temp,Node<E> node){
		//node null
		if(node==null){
			return node;
		}
		//node < elem
		if(Integer.parseInt(String.valueOf(temp.value))<Integer.parseInt(String.valueOf(node.value))){
			node.left=remove(temp,node.left);
			
		}
		else if(Integer.parseInt(String.valueOf(temp.value))<Integer.parseInt(String.valueOf(node.value))){
			node.right=remove(temp,node.right);
		}
		else{
			if(temp.elem.compareTo(node.elem)<0){
				node.left=remove(temp,node.left);
			}
			//node > elem
			else if(temp.elem.compareTo(node.elem)>0){
				node.right=remove(temp,node.right);
			}
			//2 child
			else if(node.left!=null && node.right!=null){
				//replace node with inorder successor
				temp=minNode(node.right);
				//remove inorder successor
				node.right=removeMin(node.right);
			}
			//1 child
			else{
				node=(node.left!=null)?node.left:node.right;
			}	
		}
		return node;
	}
		
	/**
	 *
	 * Mencari elemen dengan nilai paling kecil pada tree
	 * @return elemen dengan nilai paling kecil pada tree
	 *
	*/
	public E min(){
		
		E res = null;
		Node<E> minNode = minNode(root);
		
		if(minNode != null){
			
			res = minNode.elem;
			
		}
		
		return res;
		
	}
	
	/**
	  *
	  * Method untuk mengembalikan node dengan elemen terkecil pada suatu subtree
	  * Hint : Manfaatkan struktur dari binary search tree
	  * @param node root dari subtree yang ingin dicari elemen terbesarnya
	  * @return node dengan elemen terkecil dari subtree yang diinginkan
	  *
	*/
	private Node<E> minNode(Node<E> node){
		
		Node<E> res = null;
		if(node != null){
			//temp node
			Node<E> current = node;
			//getting the smallest element
			while (current.left!=null){
				current=current.left;
			}
			res=current;
		}
		
		return res;
		
	}
	
	/**
	 *
	 * Mencari elemen dengan nilai paling besar pada tree
	 * @return elemen dengan nilai paling besar pada tree
	 *
	*/
	public E max(){
		
		E res = null;
		Node<E> maxNode = maxNode(root);
		
		if(maxNode != null){
			
			res = maxNode.elem;
			
		}
		
		return res;
		
	}
	
	/**
	  *
	  * Method untuk mengembalikan node dengan elemen terbesar pada suatu subtree
	  * Hint : Manfaatkan struktur dari binary search tree
	  * @param node root dari subtree yang ingin dicari elemen terbesarnya
	  * @return node dengan elemen terbesar dari subtree yang diinginkan
	  *
	*/
	private Node<E> maxNode(Node<E> node){
		
		Node<E> res = null;
		if(node != null){
			//temp node
			Node<E> current = node;
			//getting the highest element
			while (current.right!=null){
				current=current.right;
			}
			res=current;
		}
		
		return res;
		
	}
	
	/**
	  *
	  * Mengetahui apakah sebuah objek sudah terdapat pada tree
	  * Asumsikan jika elem.compareTo(otherElem) == 0, maka elem dan otherElem merupakan objek yang sama
	  * Hint : Manfaatkan method find
	  * @param elemen yang ingin diketahui keberadaannya dalam tree
	  * @return true jika elemen ditemukan, false jika sebaliknya
	  *
	*/
	/*public boolean contains(E elem){
		if(find(elem)!=null){
			return true;
		}
		else{
			return false;
		}
	}
	*/
	/**
	  * Mengembalikan tree dalam bentuk pre-order
	  * @return tree dalam bentuk pre-order sebagai list of E
	*/
	public ArrayList<ArrayList<E>> preOrder(){
		int counter=0;
		ArrayList<ArrayList<E>> list=new ArrayList<ArrayList<E>>();
		return preOrder(root,list,counter);
		
	}
	
	/**
	  *
	  * Method helper dari preOrder()
	  * @param node pointer
	  * @param list sebagai akumulator
	  * @return kumpulan elemen dari subtree yang rootnya adalah node parameter dengan urutan pre-order
	  *
	*/
	private ArrayList<ArrayList<E>> preOrder(Node<E> node, ArrayList<ArrayList<E>> list,int counter){
		if(node==null){
			return list;
		}
		
		//adding node.elem
		ArrayList<E> inner=new ArrayList<E>();
		inner.add(node.value);
		inner.add(node.elem);
		list.add(inner);
		counter++;		if(node.left!=null){
			//getting left child
			preOrder(node.left,list,counter);
		}
		if(node.right!=null){
			//getting right child
			preOrder(node.right,list,counter);
		}
		return list;
		
	}
	
	/**
	  * Mengembalikan tree dalam bentuk post-order
	  * @return tree dalam bentuk post-order sebagai list of E
	*/
	public ArrayList<ArrayList<E>> postOrder(){
		int counter=0;
		ArrayList<ArrayList<E>> list=new ArrayList<ArrayList<E>>();
		return postOrder(root,list,counter);

	}
	
	/**
	  *
	  * Method helper dari postOrder()
	  * @param node pointer
	  * @param list sebagai akumulator
	  * @return kumpulan elemen dari subtree yang rootnya adalah node parameter dengan urutan post-order
	  *
	*/
	private ArrayList<ArrayList<E>> postOrder(Node<E> node, ArrayList<ArrayList<E>> list,int counter){
		if(node==null){
			return list;
		}
		if(node.left!=null){
			//getting left child
			postOrder(node.left,list,counter);
		}
		if(node.right!=null){
			//getting right child
			postOrder(node.right,list,counter);
		}
		//adding node.elem
		if(counter==0){
			ArrayList<E> inner=new ArrayList<E>();
			inner.add(node.value);
			inner.add(node.elem);
			list.add(inner);
			counter++;
		}
		else{
			if(list.get(counter-1).get(0).equals(node.value)){
				list.get(counter-1).add(node.elem);
				counter++;
			}
			else{
				ArrayList<E> inner=new ArrayList<E>();
				inner.add(node.value);
				inner.add(node.elem);
				list.add(inner);
				counter++;
			}
		}
		return list;
		
	}
	
	
	/**
	  * Mengembalikan tree dalam bentuk in-order secara ascending
	  * @return tree dalam bentuk in-order secara ascending sebagai list of E
	*/
	public ArrayList<ArrayList<E>> inOrderAscending(){
		int counter=0;
		ArrayList<ArrayList<E>> list=new ArrayList<ArrayList<E>>();
		return inOrderAscending(root,list,counter);
	}
	
	/**
	  *
	  * Method helper dari inOrderAscending()
	  * @param node pointer
	  * @param list sebagai akumulator
	  * @return kumpulan elemen dari subtree yang rootnya adalah node parameter dengan urutan in-order secara ascending
	  *
	*/
	private ArrayList<ArrayList<E>> inOrderAscending(Node<E> node, ArrayList<ArrayList<E>> list,int counter){
		if(node==null){
			return list;
		}
		if(node.left!=null){
			//getting left child
			inOrderAscending(node.left,list,counter);
		}
		//adding node.elem
		ArrayList<E> inner=new ArrayList<E>();
		inner.add(node.value);
		inner.add(node.elem);
		list.add(inner);
		counter++;
		if(node.right!=null){
			//getting right child
			inOrderAscending(node.right,list,counter);
		}
		return list;
		
	}
	
	
	/**
	  * Mengembalikan tree dalam bentuk in-order secara descending
	  * @return tree dalam bentuk in-order secara descending sebagai list of E
	*/
	public ArrayList<ArrayList<E>> inOrderDescending(){
		int counter=0;
		ArrayList<ArrayList<E>> list=new ArrayList<ArrayList<E>>();
		return inOrderDescending(root,list,counter);
	}
	
	/**
	  *
	  * Method helper dari inOrderDescending()
	  * @param node pointer
	  * @param list sebagai akumulator
	  * @return kumpulan elemen dari subtree yang rootnya adalah node parameter dengan urutan in-order descending
	  *
	*/
	
	private ArrayList<ArrayList<E>> inOrderDescending(Node<E> node, ArrayList<ArrayList<E>> list,int counter){
		if(node==null){
			 return list;
		}
		//getting right child
		inOrderDescending(node.right,list,counter);

		//adding node.elem
		ArrayList<E> inner=new ArrayList<E>();
		inner.add(node.value);
		inner.add(node.elem);
		list.add(inner);
		counter++;

		//getting left child
		inOrderDescending(node.left,list,counter);
		return list;
	}
}