 $( document ).ready(function() {
	var button_8 = $('button:contains("8")');
	var button_4 = $('button:contains("4")');
	var button_9 = $('button:contains("9")');
	
	var button_add = $('button:contains("+")');
	var button_sub = $('button:contains("-")');
	var button_mul = $('button:contains("*")');
	var button_div = $('button:contains("/")');

	var button_clear = $('button:contains("AC")');
	var button_res = $('button:contains("=")');

	var button_sin = $('button:contains("sin")');
	var button_log = $('button:contains("log")');
	var button_tan = $('button:contains("tan")');
	
	QUnit.test( "Addition Test", function( assert ) {
		button_8.click();
		button_add.click();
		button_4.click();
		button_res.click();
		assert.equal( $('#print').val(), 12, "8 + 4 must be 12" );
		button_clear.click();
	});
	
	QUnit.test( "Multiply Test", function( assert ) {
		button_8.click();
		button_mul.click();
		button_4.click();
		button_res.click();
		assert.equal( $('#print').val(), 32, "8 * 4 must be 32" );
		button_clear.click();
	});

	QUnit.test( "Division Test", function( assert ) {
		button_8.click();
		button_div.click();
		button_4.click();
		button_res.click();
		assert.equal( $('#print').val(), 2, "8 / 4 must be 2" );
		button_clear.click();
	});

	//QUnit Challange SIN LOG TAN test
	QUnit.test( "Sin Test", function( assert ) {
		button_9.click();
		button_sin.click();
		assert.equal( $('#print').val(), 0.4121184852417566, "sin(9) must be 0.4121184852417566" );
		button_clear.click();
	});
	
	QUnit.test( "Log Test", function( assert ) {
		button_9.click();
		button_log.click();
		assert.equal( $('#print').val(), 0.9542425094393249, "log(9) must be 0.9542425094393249" );
		button_clear.click();
	});
	
	QUnit.test( "Tan Test", function( assert ) {
		button_9.click();
		button_tan.click();
		assert.equal( $('#print').val(), -0.45231565944180985, "tan(9) must be -0.45231565944180985" );
		button_clear.click();
	});
});
