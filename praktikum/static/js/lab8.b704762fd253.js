window.fbAsyncInit = function() {
	FB.init({
		appId      : '2023857921165524',
		cookie     : true,
		xfbml      : true,
		version    : 'v2.11'
	});
};

(function(d, s, id){
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) {return;}
	js = d.createElement(s); js.id = id;
	js.src = "https://connect.facebook.net/en_US/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function getUserData(){
	FB.getLoginStatus(function(response) {
		if (response.status === 'connected') {
			FB.api('/me?fields=id,name', 'GET', function(response){
			console.log(response);
			});
		}
	});
}

function facebookLogin(){
	FB.login(function(response){
		console.log(response);
		}, {scope:'public_profile'})
}

function postFeed(){
	var message = "Hello World!";
	FB.api('/me/feed', 'POST', {message:message});
}

function facebookLogout(){
	FB.getLoginStatus(function(response) {
		if (response.status === 'connected') {
			FB.logout();
		}
	});
}
