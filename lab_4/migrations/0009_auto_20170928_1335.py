# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-09-28 06:35
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('lab_4', '0008_auto_20170928_1324'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2017, 9, 28, 6, 35, 23, 688485, tzinfo=utc)),
        ),
    ]
