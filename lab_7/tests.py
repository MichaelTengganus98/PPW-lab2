from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .models import Friend
from .views import index, paginate_page, friend_list, add_friend, delete_friend, validate_npm, model_to_dict
from .api_csui_helper.csui_helper import CSUIhelper
# Create your tests here.

class Lab7UnitTest(TestCase):
    def test_lab_7_url_is_exist(self):
        response = Client().get('/lab-7/')
        self.assertEqual(response.status_code, 200)

    def test_lab7_using_index_func(self):
        found = resolve('/lab-7/')
        self.assertEqual(found.func, index)

    def test_friend_list_url_is_exist(self):
        response = Client().get('/lab-7/friend-list/')
        self.assertEqual(response.status_code, 200)

    def test_get_friend_list_data_url_is_exist(self):
        response = Client().get('/lab-7/get-friend-list/')
        self.assertEqual(response.status_code, 200)

    def test_paginate_pageNotAnInteger(self):
        page = "Tidak Ada Page"
        data_list = ['hehe','hoho','hihi']
        paginator = paginate_page(page, data_list)
        self.assertEqual(paginator['page_range'][0], 1)

    def test_paginate_pageEmpty(self):
        page = 10
        data_list = []
        paginator = paginate_page(page, data_list)
        self.assertEqual(paginator['page_range'][0], 1)

    def test_model_to_dict(self):
        data = Friend.objects.create(friend_name='Hoho', npm='1231231231')
        dataNow = model_to_dict(data)
        self.assertEqual(type(dataNow), str)
    
    def test_auth_param_dict(self):
        csui_helper = CSUIhelper()
        auth_param = csui_helper.instance.get_auth_param_dict()
        self.assertEqual(auth_param['client_id'], csui_helper.instance.get_auth_param_dict()['client_id'])

    def test_add_friend(self):
        response_post = Client().post('/lab-7/add-friend/',{'name': "nama",'npm' : "npm"})
        self.assertEqual(response_post.status_code, 200)

    def test_invalid_sso_raise_exception(self):
        username = "hehe"
        password = "hehe123"
        csui_helper = CSUIhelper()
        with self.assertRaises(Exception) as context:
            csui_helper.instance.get_access_token(username, password)
        self.assertIn(username, str(context.exception))

    def test_validate_npm(self):
        response = self.client.post('/lab-7/validate-npm/')
        html_response = response.content.decode('utf8')
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(html_response, {'is_taken':False})

    def test_delete_friend(self):
        friend = Friend.objects.create(friend_name="Hehe", npm="1231231231")
        response = Client().post('/lab-7/delete-friend/' + str(friend.id) + '/')
        self.assertEqual(response.status_code, 302)
        self.assertNotIn(friend, Friend.objects.all())


        
